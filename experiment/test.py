import numpy
import pandas as pd

from subscriber.loadtester import LoadTester


def tuple_generator_diagonal():
    d = 1
    while True:
        d += 1
        for x in range(1, d):
            yield (x, d - x)


def tuple_generator_orthogonal():
    level = 0
    while True:
        level += 1
        for delta in range(1, level):
            yield (delta, level)  # Return next item on the vertical line
            yield (level, delta)  # Return next item on the horizontal item

        yield (level, level)  # Return the corner


if __name__ == "__main__":
    gen = tuple_generator_orthogonal()

    while True:
        (x, y) = next(gen)
        print((x, y))

        if x >= 10:
            print("Finished")
            break

        sub_amount, pub_count, pub_speedup = 2 ** x, 2 ** y, (2 ** y) * 100
        data = LoadTester().run(sub_amount, pub_count, pub_speedup)

        s = pd.Series(data)
        statistics = s.describe()
        print(statistics)

        with open('results.csv', 'a') as file:
            a = numpy.array(statistics).ravel()[None]
            file.write(",".join([str(sub_amount), str(pub_count), str(pub_speedup), ""]))
            numpy.savetxt(file, a, delimiter=',')
