import os
import sqlite3
import time
import uuid
from datetime import datetime

from shared.mqtt_client import Client

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


class Publisher:

    def __init__(self) -> None:
        self.id = str(uuid.uuid4())[0:8]

        conn = sqlite3.connect(os.path.join(BASE_DIR, 'publisher', 'data', 'data.db'))
        c = conn.cursor()

        boards = c.execute('SELECT boardid FROM data GROUP BY boardid')

        self.publishers = {}
        for board in boards:
            board_id = board[0]

            client = Client(self.id + "-" + board_id)
            client.connect()

            self.publishers[board_id] = client

        conn.close()

    def run(self, amount: int, speedup: float):
        conn = sqlite3.connect(os.path.join(BASE_DIR, 'publisher', 'data', 'data.db'))
        c = conn.cursor()

        data = c.execute('SELECT ts as "ts [timestamp]", boardid, temp FROM data ASC LIMIT ?', (amount,))

        virtual_time = 0
        for row in data:
            ts = datetime.fromisoformat(row[0])
            unix = time.mktime(ts.timetuple())

            board_id = row[1]
            avg_temp = row[2]
            self.publishers[board_id].publish('session/%s/board/%s/avg_temp' % (self.id, board_id), avg_temp)

            if virtual_time == 0:
                virtual_time = unix

            if unix > virtual_time:
                wait, virtual_time = (unix - virtual_time) / speedup, unix

                # print("Waiting till: %s (%s IRL seconds)" % (ts, wait))
                time.sleep(wait)

        conn.close()
