import csv
import datetime
import os.path
import sqlite3

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


def read_data() -> (list, csv.reader):
    with open(os.path.join(BASE_DIR, 'publisher', 'data', 'data.csv'), 'rt') as csvfile:
        csvreader = csv.reader(csvfile)

        (headers, reader) = next(csvreader, None), csvreader

        timestamp_index = headers.index("timestamp")
        boardid_index = headers.index("boardid")
        temp_avg_index = headers.index("temp_avg")

        query = []
        for row in reader:
            dt = datetime.datetime.strptime(row[timestamp_index], '%m/%d/%Y %I:%M:%S %p')
            boardid = row[boardid_index]
            avg_temp = float(row[temp_avg_index])

            query.append((dt, boardid, avg_temp,))

        return query


q = read_data()

conn = sqlite3.connect(os.path.join(BASE_DIR, 'publisher', 'data', 'data.db'))
c = conn.cursor()

c.execute('''CREATE TABLE IF NOT EXISTS data
            (ts TIMESTAMP, boardid TEXT, temp REAL)''')

c.executemany('INSERT INTO data VALUES (?, ?, ?)', q)

conn.commit()
conn.close()
