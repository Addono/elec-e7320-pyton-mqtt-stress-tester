import os
import sqlite3
import ssl
from datetime import datetime

import paho.mqtt.client as paho

import cert
from shared.config import MQTT_BROKER_DOMAIN, MQTT_BROKER_PORT

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


class Client(paho.Client):

    def __init__(self, client_id="", clean_session=True, userdata=None, protocol=paho.MQTTv311, transport="tcp", tls=True):
        super().__init__(client_id, clean_session, userdata, protocol, transport)
        self.tls = tls
        if tls:
            self.tls_set(cert.CA_CERTS, cert_reqs=ssl.CERT_NONE)

    def connect(self, host=MQTT_BROKER_DOMAIN, port=MQTT_BROKER_PORT, keepalive=60, bind_address=""):
        res = super().connect(host, port, keepalive, bind_address)
        self.loop_start()
        return res

    def connect_async(self, host=MQTT_BROKER_DOMAIN, port=8883, keepalive=60, bind_address=""):
        return super().connect_async(host, port, keepalive, bind_address)

    # Override to set the default QOS to 2
    def publish(self, topic, payload=None, qos=2, retain=False):
        return super().publish(topic, payload, qos, retain)

    # Override to set the default QOS to 2
    def subscribe(self, topic, qos=2):
        return super().subscribe(topic, qos)

    def subscribe_and_store(self, topic, qos=2):
        res = self.subscribe(topic, qos)

        # Create the table in case it didn't exist yet.
        db = self._open_db()
        c = db.cursor()
        c.execute("""CREATE TABLE IF NOT EXISTS data (ts TIMESTAMP, topic TEXT, payload TEXT)""")
        db.commit()
        db.close()

        # Add a listener to write to the database.
        self.message_callback_add(topic, lambda client, userdata, msg: self._insert_into_db(msg.topic, msg.payload))

        return res

    def _insert_into_db(self, topic, message):
        db = self._open_db()
        c = db.cursor()
        c.execute('INSERT INTO data VALUES (?, ?, ?)', (datetime.now(), topic, message))
        db.commit()
        db.close()

    def _open_db(self):
        return sqlite3.connect(os.path.join(BASE_DIR, 'subscriber', 'data', 'client-%s.db' % self._client_id.decode('ascii')))
