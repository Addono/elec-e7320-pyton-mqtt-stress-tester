import threading
import time
from unittest.mock import MagicMock

from publisher.publisher import Publisher
from test.test_broker import generate_connected_clients


class LoadTester:

    def run(self, sub_amount: int, pub_count: int, pub_speedup: float):
        subscribers = generate_connected_clients(sub_amount)
        publishers = Publisher()

        # Subscribe all subs
        for s in subscribers:
            s.subscribe("session/%s/#" % publishers.id)
            s.on_message = MagicMock()

        thread = threading.Thread(target=publishers.run, args=(pub_count, pub_speedup), kwargs={})
        thread.start()

        thread.join()

        time.sleep(4)  # Make sure that the last subs have some time to catch up.

        amount_of_messages = []
        for s in subscribers:
            amount_of_messages.append(s.on_message.call_count)

        return amount_of_messages
