import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

CA_CERTS = os.path.join(BASE_DIR, "broker", "config", "ca_certificates", "m2mqtt_ca.crt")
