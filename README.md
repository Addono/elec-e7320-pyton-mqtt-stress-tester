# MQTT Stress Tester
Requires Python 3.7

## Setup

### Broker
By default the broker is deployed using Dockercompose. Start the broker using:
```bash
cd broker
docker-compose up
```

Certificates where generated following [this](https://mcuoneclipse.com/2017/04/14/enable-secure-communication-with-tls-and-the-mosquitto-broker/) guide.

## Tests
First install all required Python packages by running:
```sh
pip install -r requirements.txt
```

After which the tests can be executed by invoking PyTest:
```
pytest
```