import time

from test.test_broker import generate_topic, generate_connected_client


def test_stored_data():
    """
    Tests if the subscriber client stores its data in a database.
    :return:
    """
    topic1 = generate_topic()
    message = b"message1"

    publisher = generate_connected_client()
    subscriber = generate_connected_client()

    # Subscribe
    subscriber.subscribe_and_store(topic1)

    publisher.publish(topic1, message)

    time.sleep(3)

    # Read all data directly from the database
    with subscriber._open_db() as db:
        cursor = db.cursor()
        data = cursor.execute("""SELECT topic, payload FROM data""").fetchall()

    # Assert that the only row in the database is the one from the message which was just published
    assert len(data) == 1
    assert data[0] == (topic1, message)

    # Tear-down
    subscriber.disconnect()
    publisher.disconnect()
