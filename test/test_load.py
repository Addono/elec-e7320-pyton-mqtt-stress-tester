from subscriber.loadtester import LoadTester


def test_small():
    sub_amount = 10
    pub_count = 10

    result = LoadTester().run(sub_amount, pub_count, 1000)

    assert result == [pub_count] * sub_amount
