from experiment.test import tuple_generator_orthogonal


def test_tuple_generator_orthogonal():
    gen = tuple_generator_orthogonal()

    tuples = [next(gen) for _ in range(16)]

    assert tuples == [(1, 1),
                      (1, 2), (2, 1), (2, 2),
                      (1, 3), (3, 1), (2, 3), (3, 2), (3, 3),
                      (1, 4), (4, 1), (2, 4), (4, 2), (3, 4), (4, 3), (4, 4)
                      ]
