import time
import uuid
from typing import List
from unittest.mock import MagicMock

from shared.mqtt_client import Client

def generate_topic():
    return "demo/topic/" + str(uuid.uuid4())


def generate_connected_client():
    return generate_connected_clients(1)[0]


def generate_connected_clients(amount: int) -> List[Client]:
    assert amount > 0

    unique = str(uuid.uuid4())[0:8]
    clients = [Client("tc-" + unique + "-" + str(i)) for i in range(amount)]

    # Connect all clients
    for c in clients:
        c.connect()

    return clients


def test_connect_tls():
    client = Client()
    client.on_connect = MagicMock()
    client.connect()

    time.sleep(1)

    client.on_connect.assert_called_once()
    client.disconnect()


def test_connect_without_tls():
    client = Client(tls=False)
    client.on_connect = MagicMock()
    client.connect()

    time.sleep(1)

    client.on_connect.assert_not_called()
    client.disconnect()


def test_publish():
    topic = generate_topic()
    clients = generate_connected_clients(10)

    # Divide all clients into groups
    publisher, nonsubscriber, subscribers = clients[0], clients[1], clients[2:]

    # Subscribe the subscribers
    for sub in subscribers:
        sub.subscribe(topic=topic)

    # Mock the on_message listener
    for sub in [nonsubscriber] + subscribers:
        sub.on_message = MagicMock()

    # Publish a message
    publisher.publish(topic=topic, payload="test")

    time.sleep(4)

    # Ensure that the not-subscribed client hasn't received anything.
    nonsubscriber.on_message.assert_not_called()

    # Check if all the other subscribers received something.
    for sub in subscribers:
        sub.on_message.assert_called_once()

    # Disconnect all clients
    for c in clients:
        c.disconnect()


def test_unsubscribe():
    topic1 = generate_topic()
    topic2 = generate_topic()
    publisher = generate_connected_client()
    subscriber = generate_connected_client()

    # Subscribe to both topics
    subscriber.subscribe(topic1)
    subscriber.subscribe(topic2)
    subscriber.on_message = MagicMock()

    publisher.publish(topic1, "message1")
    publisher.publish(topic2, "message2")

    time.sleep(4)

    # Check that both messages where received
    assert subscriber.on_message.call_count == 2

    # Unsubscribe from one of the topics
    subscriber.unsubscribe(topic1)

    publisher.publish(topic1, "message1")
    publisher.publish(topic2, "message2")

    time.sleep(4)

    # Assert that the client now only receives messages on one of the topics
    assert subscriber.on_message.call_count == 3

    # Tear-down
    subscriber.disconnect()
    publisher.disconnect()


def test_parallel_interference():
    """
    Tests if three pairs of pub-sub clients can communicate over the same broker without interference.
    :return:
    """
    parallel_interference_test(parallel_count=3)


def parallel_interference_test(parallel_count):
    topics = [generate_topic() for _ in range(parallel_count)]
    publishers = generate_connected_clients(parallel_count)
    subscribers = generate_connected_clients(parallel_count)
    publish_count = [0] * parallel_count

    def assert_valid(publish_count: list, subscribers: list):
        """
        Checks if the publish count matches the amount of messages received by the corresponding subscriber.
        """
        for subscriber, count in zip(subscribers, publish_count):
            assert subscriber.on_message.call_count == count

    # Subscribe to all topics
    for subscriber, topic in zip(subscribers, topics):
        subscriber.subscribe(topic)
        subscriber.on_message = MagicMock()

    # Ensure that we start in a valid situation
    assert_valid(publish_count, subscribers)

    # Publish one message with each publisher and ensure that the corresponding subscriber receives the message
    for key, (publisher, topic) in enumerate(zip(publishers, topics)):
        publisher.publish(topic, "message-%s" % key)
        publish_count[key] += 1

        # Give the message some time to be processed.
        time.sleep(3)

        # Check if the message is correctly received
        assert_valid(publish_count, subscribers)

    # Tear-down
    for client in publishers + subscribers:
        client.disconnect()
